<?php
/**
 * Plugin Name: Bookcraft
 * Author: ehsaan
 * Plugin URI: https://gitlab.com/ehsaan/bookcraft
 * Author URI: https://ehsaan.me
 * License: MIT
 * Description: Nothing serious really.
 */

namespace BookCraft;

\add_action( 'plugins_loaded', [ BookCraft::getInstance(), 'setup' ] );

/**
 * Load the core manually.
 */
require_once 'includes/Core.php';
\register_activation_hook( __FILE__, [ '\\BookCraft\\Core', 'onActivation' ] );

class BookCraft
{
    /**
     * Plugin instance.
     * 
     * @see getInstance()
     * @var object
     */
    protected static $instance = null;

    /**
     * Universal path to plugin directory.
     * 
     * @var string
     */
    public $plugin_url = '';

    /**
     * Local path to plugin directory.
     * 
     * @var string
     */
    public $plugin_path = '';

    /**
     * Access plugin working instance.
     * 
     * @wp-hook plugins_loaded
     * @return object
     */
    public static function getInstance() {
        null === self::$instance and self::$instance = new self;
        return self::$instance;
    }

    /**
     * Class constrcutor.
     * Intentionally left empty and public.
     */
    public function __construct() {}

    /**
     * Setup plugin.
     * 
     * @wp-hook plugins_loaded
     * @return void
     */
    public function setup() {
        $this->plugin_url = \plugins_url( __FILE__ );
        $this->plugin_path = \plugin_dir_path( __FILE__ );
        $this->loadLanguages( 'bookcraft' );
        
        spl_autoload_register( [ $this, 'autoload' ] );

        // Semantic actions, thanks to PSR loading
        Types::postType();
        Types::taxonomies();
        Admin\UI::addIsbnBox();
        Admin\UI::addDetailsPage();
        Admin\Hooks::init();
    }

    /**
     * Loads the language files
     * 
     * @param string $domain
     */
    public function loadLanguages( $domain ) {
        load_plugin_textdomain( $domain, false, $this->plugin_path . '/languages' );
    }

    /**
     * Autoload required files
     * 
     * @param string $file
     */
    public function autoload( $file ) {
        $file = str_replace( '\\', DIRECTORY_SEPARATOR, $file );
        $file = str_replace( 'BookCraft/', '', $file );

        if ( ! class_exists( $file ) ) {
            $fullPath = $this->plugin_path . 'includes/' . $file . '.php';

            if ( file_exists( $fullPath ) )
                require_once $fullPath;
        }
    }
}