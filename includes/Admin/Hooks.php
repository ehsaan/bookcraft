<?php
namespace BookCraft\Admin;

class Hooks {
    /**
     * Register the hook(s)
     */
    public static function init() {
        add_action( 'save_post', [ self::class, 'saveBookIsbn' ] );
    }

    /**
     * Save the Book ISBN when saving the book.
     */
    public static function saveBookIsbn( $post_id ) {
        $nonce          =   $_POST['isbn_nonce'] ?? '';
        $nonce_action   =   'save_isbn_action';

        if ( ! $nonce || ! wp_verify_nonce( $nonce, $nonce_action ) || ! current_user_can( 'edit_post', $post_id ) ||
            wp_is_post_autosave( $post_id ) || wp_is_post_revision( $post_id ) )
            return;
        
        global $wpdb;
        
        $isbn = sanitize_text_field( $_POST['bookIsbn'] );
        
        /**
         * Check if record exists
         */
        $prevRow = $wpdb->get_row( "SELECT id FROM {$wpdb->prefix}books_info WHERE post_id={intval($post_id)}" );
        if ( ! $prevRow ) {
            $wpdb->insert(
                $wpdb->prefix . 'books_info',
                [
                    'post_id'   =>  $post_id,
                    'isbn'      =>  $isbn,
                ],
                [ '%d', '%s' ]
            );
        } else {
            $wpdb->update(
                $wpdb->prefix . 'books_info',
                [
                    'isbn'      =>  $isbn,
                ],
                [
                    'post_id'   =>  $post_id,
                ]
            );
        }
    }
}