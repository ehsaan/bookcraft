<?php

namespace BookCraft;

/**
 * Core class only contains "onActivation"
 * method for now.
 */
class Core {
    /**
     * Delta the table "books_info" into WP
     * database upon activation.
     */
    public static function onActivation() {
        global $wpdb;

        $tableName = $wpdb->prefix . 'books_info';
        $charsetCollate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $tableName (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            post_id int(11) NOT NULL,
            isbn varchar(64) DEFAULT '' NOT NULL,
            PRIMARY KEY (id)
        ) $charsetCollate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        \dbDelta( $sql );
    }
}