<?php
namespace BookCraft\Admin;

class UI {
    /**
     * Add ISBN meta box.
     */
    public static function addIsbnBox() {
        \add_action( 'add_meta_boxes', function() {
            \add_meta_box(
                'bookIsbn',
                'ISBN',
                [ self::class, 'renderIsbnBox' ],
                'book',
                'normal',
                'high'
            );
        } );
    }

    /**
     * Render meta box.
     */
    public static function renderIsbnBox() {
        wp_nonce_field( 'save_isbn_action', 'isbn_nonce' );
        
        global $post_id;
        $current_isbn = '';

        if ( $post_id ) {
            global $wpdb;
            $current_isbn = $wpdb->get_var( "SELECT isbn FROM {$wpdb->prefix}books_info WHERE post_id={$post_id} LIMIT 1" );
        }

        ?>
        <input type="text" name="bookIsbn" value="<?php echo $current_isbn; ?>" placeholder="<?php _e( 'Book ISBN', 'bookcraft' ); ?>">
        <?php
    }

    /**
     * Add details page.
     */
    public static function addDetailsPage() {
        add_action( 'admin_menu', function() {
            add_submenu_page(
                'edit.php?post_type=book',
                __( 'Book Details', 'bookcraft' ),
                __( 'Details', 'bookcraft' ),
                'edit_posts',
                'book-details',
                [ self::class, 'renderListTable' ] 
            );
        } );
    }

    /**
     * Render the list table.
     */
    public static function renderListTable() {
        $table = new DetailsTable();
        echo '<div class="wrap"><h2>' . __( 'Books Details', 'bookcraft' ) . '</h2>';
        $table->prepare_items();
        $table->display();
        echo '</div>';
    }
}