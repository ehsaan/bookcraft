<?php
namespace BookCraft\Admin;

if ( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class DetailsTable extends \WP_List_Table {
    /**
     * Columns headers
     */
    public function get_columns() {
        $columns = [
            'post_id'       =>  __( 'Book', 'bookcraft' ),
            'isbn'          =>  __( 'ISBN', 'bookcraft' ),
        ];
        return $columns;
    }

    /**
     * Retrieve each column value.
     */
    public function column_default( $item, $column_name ) {
        if ( $column_name == 'post_id' )
            return '<a href="post.php?action=edit&post=' . $item[ 'post_id' ] . '">' . get_the_title( $item[ 'post_id' ] ) . '</a>';
        else
            return $item[ $column_name ];
    }

    /**
     * Prepare the data from database.
     */
    public function prepare_data() {
        global $wpdb;
        return $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}books_info ORDER BY id DESC", ARRAY_A );
    }
    
    /**
     * Prepare the List_Table.
     */
    public function prepare_items() {
        $columns = $this->get_columns();
        $hidden = [];
        $sortable = [];
        $this->_column_headers = [ $columns, $hidden, $sortable ];
        $this->items = $this->prepare_data();
    }
}