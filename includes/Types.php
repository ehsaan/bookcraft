<?php
namespace BookCraft;

class Types {
    /**
     * Registers the "book" post type.
     */
    public static function postType() {
        add_action( 'init', function() {
            register_post_type( 'book', [
                'name'          =>  __( 'Books', 'bookcraft' ),
                'public'        =>  true,
                'show_ui'       =>  true,
                'menu_icon'     =>  'dashicons-book-alt',
                'supports'      =>  [ 'title' ],
                'taxonomies'    =>  [ 'book-author', 'book-publisher' ],
                'query_var'     =>  'book',
            ] );
        } );
    }

    /**
     * Registers the "book-author" and "book-publisher"
     * taxonomies.
     */
    public static function taxonomies() {
        add_action( 'init', function() {
            register_taxonomy( 'book-author', 'book', [
                'labels'            =>  [
                    'name'          =>  __( 'Authors', 'bookcraft' ),
                ],
                'public'            =>  true,
                'show_ui'           =>  true,
                'show_admin_column' =>  true,
            ] );

            register_taxonomy( 'book-publisher', 'book', [
                'labels'            =>  [
                    'name'          =>  __( 'Publishers', 'bookcraft' ),
                ],
                'public'            =>  true,
                'show_ui'           =>  true,
                'show_admin_column' =>  true,
            ] );
        } );
    }
}